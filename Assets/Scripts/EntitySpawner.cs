﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Spawns specified prefab every <c>spawnTimer</c> seconds.
/// </summary>
public class EntitySpawner : MonoBehaviour {

	public GameObject entityPrefab;
	public float spawnTimer = 1f;
	private float startingSpawnTimer;

	private void Start () {
		startingSpawnTimer = spawnTimer;
	}

	private void SpawnEntity ()
	{
		Instantiate (entityPrefab, transform.position, transform.rotation);
		spawnTimer = startingSpawnTimer;
	}

	void Update () {
		spawnTimer -= Time.deltaTime;

		if( spawnTimer <= 0 ) {
			SpawnEntity ();
		}
	}
}
