﻿using UnityEngine;
using System.Collections;

public class TiltOnHorizontalInput : MonoBehaviour {

	// The higher the value, faster it will rotates
	public float tiltSpeed = 90f;
	public float tiltRotationLimit = 45f;
	private float tiltRotationLimitNegative;

	void Start () {
		// Ensure signs are where they belong
		tiltRotationLimitNegative = - Mathf.Abs(tiltRotationLimit);
		tiltRotationLimit = Mathf.Abs(tiltRotationLimit);
	}

	void Update () {
		// Nice effect to show the player tilting to the sides

		Quaternion rot = Quaternion.identity;
		float tilt = rot.eulerAngles.y;

		// Apply rotation depending on horizontal movement
		tilt -= Input.GetAxis ("Horizontal") * tiltSpeed * Time.deltaTime;

		// Make sure tilt don't exceed limits
		if( tilt < tiltRotationLimitNegative ) {
			tilt = tiltRotationLimitNegative;
		} else if( tilt > tiltRotationLimit ) {
			tilt = tiltRotationLimit;
		}
		
		// Create the new rotation on "Y" axis
		rot = Quaternion.Euler (0, tilt, 0);
		transform.rotation = rot;
	}
}
