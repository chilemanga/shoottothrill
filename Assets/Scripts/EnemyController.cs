﻿using UnityEngine;
using System.Collections;

public class EnemyController : PlayerFinder {

	// Default score given by enemy kills. Modify this for enemies created.
	public int score = 10;
	public GameObject explosionContainer;

	private PowerUpSpawner powerUpSpawnerComponent;

	void Awake () {
		// Autogenerate PowerUpSpawner if no such component have been added
		powerUpSpawnerComponent = GetComponent<PowerUpSpawner>();
		if (powerUpSpawnerComponent == null) {
			gameObject.AddComponent<PowerUpSpawner>();
		}

		// Subscribe to enemy died event
		GameManager.instance.OnEnemyDied += HandleOnEnemyDied;
	}

	void HandleOnEnemyDied (EnemyController enemyController)
	{
		if( enemyController == this) {
			StartCoroutine( enemyController.Die () );
		}
	}

	public IEnumerator Die()
	{
		// Trigger explosions
		explosionContainer.SetActive(true);

		yield return new WaitForSeconds(0.25f);

		if (powerUpSpawnerComponent != null) {
			// Spawn a power up, if possible
			powerUpSpawnerComponent.enabled = true;
		}

		if( gameObject != null ) {
			Destroy ( gameObject );
		}
	}
}
