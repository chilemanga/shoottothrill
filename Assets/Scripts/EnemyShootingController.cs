﻿using UnityEngine;
using System.Collections;

public class EnemyShootingController : ShootingController {

	public Vector3 bulletOffset = new Vector3(0, 1f, 0);

	protected override void Update () {
		cooldownCountdown -= Time.deltaTime;
		
		// Shoot automatically when countdown reaches zero
		if( cooldownCountdown <= 0 ) {
			cooldownCountdown = fireRatio;

			Vector3 offset = transform.rotation * bulletOffset;

			Instantiate( bulletPrefab, transform.position + offset, transform.rotation );
		}
	}
}
