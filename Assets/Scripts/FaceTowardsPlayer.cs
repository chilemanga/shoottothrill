﻿using UnityEngine;
using System.Collections;

public class FaceTowardsPlayer : PlayerFinder {

	/// <summary>
	/// The rotation speed. It will rotate that amount of degrees, per second.
	/// </summary>
	public float rotationSpeed = 210f;

	private Transform player { get { return target; } }

	void Update () {
		if( player == null ) {
			// Can't rotate to an inexistant player!
			Debug.Log ("NO PLAYER FOUND?!?!");
			return;
		}

		// Get the direction in which to look at, normalized
		Vector3 direction = (player.position - transform.position);
		direction.Normalize ();

		// Just rotate Z axis
		float z = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg - 90f;

		transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.Euler (0, 0, z), rotationSpeed * Time.deltaTime);
	}
}
