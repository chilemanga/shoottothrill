﻿using UnityEngine;
using System.Collections;

public class ShootingController : MonoBehaviour {

	public GameObject bulletPrefab;
	public float fireRatio = 0.15f;

	// We want to set this in the inspector, but we don't want to inherit this member
	[SerializeField]
	private PlayerController playerController;

	protected float cooldownCountdown = 0;

	protected virtual void Update () {
		if( playerController == null ) {
			Debug.Log ("No player to shoot from! Make sure to reference the player through the inspector on Player Controller field.");
			return;
		}
		cooldownCountdown -= Time.deltaTime;

		// Shoot when pressing space bar!
		if( Input.GetKey(KeyCode.Space) && cooldownCountdown <= 0 && !playerController.isDead ) {
			cooldownCountdown = fireRatio;

			foreach (GameObject cannon in playerController.GetCurrentCannons ()) {
				// Instantiate bullet prefab (with no rotation when firing from the player!) from every cannon
				Instantiate( bulletPrefab, cannon.transform.position, cannon.transform.rotation );
			}
		}
	}
}
