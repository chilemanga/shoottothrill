﻿using UnityEngine;
using System.Collections;

public class MoveForwardBehaviour : MonoBehaviour {

	public float speed = 10f;

	void Update () {

		Vector3 pos = transform.position;

		// Forward means applying velocity just in the vertical axis
		Vector3 velocity = new Vector3( 0, speed * Time.deltaTime, 0 );
		// Consider rotation to use this script on enemies bullets, PEW!
		pos += transform.rotation * velocity;

		transform.position = pos;
	}
}
