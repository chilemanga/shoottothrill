using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T: class {
	protected static T _instance = null;
	public static T instance {
		get {
			if( _instance == null ) {
				_instance = SingletonManager.gameObject.AddComponent(typeof(T)) as T;
			}
			return _instance;
		}
	}
	
	public static void Instantiate() {
		_instance = instance;
	}
	
	public Singleton() {
		_instance = this as T;
	}
}

public class SingletonManager {
	private static GameObject _gameObject = null;
	public static GameObject gameObject {
		get {
			if(_gameObject == null) {
				// You'll find singletons under -SingletonManager name on the hierarchy
				_gameObject = new GameObject("-SingletonManager");
				Object.DontDestroyOnLoad(_gameObject);
			}
			return _gameObject;
		}
	}
}