﻿
public enum EnumLayers {
	Default = 0,
	Player = 8,
	Enemy = 9,
	PlayerBullet = 10,
	EnemyBullet = 11,
	Invulnerable = 20
}
