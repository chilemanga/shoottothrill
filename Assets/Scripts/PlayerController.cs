﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

	#region Editor set values
	[Header ("Tunning values")]
	/// <summary>
	/// The player speed. The higher the value, faster the player will move!
	/// </summary>
	public float playerSpeed;
	public float playerRadius = 0.5f;
	public float timeToRespawn = 1f;
	public float invulnerableTimeAfterDeath = 2f;
	public Vector3 startPosition = new Vector3();
	[Header ("Objects references")]
	public Camera mainCamera;
	public GameObject graphicContainer;
	public FlashingObject flashingComponent;
	public GameObject currentFireSpot;
	public List<GameObject> fireSpots = new List<GameObject>();
	public GameObject explosion;

	#endregion

	#region Private fields and properties

	// HP is encapsulated in OntriggerEnter2DHandler... because reasons xD
	private OnTriggerEnter2DHandler cachedTriggerHandlerController;
	private int _hp;
	public int hp
	{
		set
		{
			_hp = value;
			if( cachedTriggerHandlerController != null )
			{
				cachedTriggerHandlerController.hp = _hp;
			}
		}
		get
		{
			if( cachedTriggerHandlerController == null )
			{
				// Cache the component for efficiency
				cachedTriggerHandlerController = GetComponent<OnTriggerEnter2DHandler>();
			}

			if( cachedTriggerHandlerController != null )
			{
				_hp = cachedTriggerHandlerController.hp;
				return _hp;
			}
			else
			{
				_hp = 1;
			}
			return _hp;
		}
	}

	private int startingHP;
	private float startingPlayerSpeed;

	private bool isRespawning = false;
	public bool isDead { get{ return hp <= 0; } }

	#endregion

	private void Start () {
		// Ensure a camera is assigned
		if( mainCamera == null ) {
			mainCamera = Camera.main;
		}

		// Move the player to the configured starting position
		transform.position = startPosition;

		// Save configured HP and speed for when player dies/revives
		startingHP = hp;
		startingPlayerSpeed = playerSpeed;

		GameManager.instance.OnPlayerDied += HandleOnPlayerDied;
	}

	private void HandleOnPlayerDied ()
	{
		if( isRespawning ) {
			return;
		}
		Debug.Log ("OMG! I've died... lives left: "+GameManager.instance.playerLives);

		// Reset cannon power
		currentFireSpot = fireSpots[0];

		// Reset speed
		playerSpeed = startingPlayerSpeed;

		// BOOOM! Show an explosion!
		explosion.SetActive ( true );

		// Instead of destroying it, just disable its graphic container
		graphicContainer.SetActive(false);

		if( !GameManager.instance.gameOver ) {
			StartCoroutine( Respawn() );
		}
	}

	private IEnumerator Respawn() {

		// Disable camera following
//		FollowHorizontalSmoothly followingCameraComponent = mainCamera.GetComponent<FollowHorizontalSmoothly>();
//		if( followingCameraComponent != null ) {
//			followingCameraComponent.enabled = false;
//		}

		yield return new WaitForSeconds( timeToRespawn );
		
		// Move the player to the original position
		transform.position = startPosition;

		// Enable the effect of invulnerability
		flashingComponent.enabled = true;

		// Move it to a layer in which it doesn't collide
		gameObject.layer = (int)EnumLayers.Invulnerable;

		// Activate it again to make it visible and start flashing
		graphicContainer.SetActive(true);

		// Revive!
		hp = startingHP;

		yield return new WaitForSeconds( invulnerableTimeAfterDeath );

		// Disable the effect of invulnerability again
		flashingComponent.enabled = false;

		// Move it to a its normal layer again
		gameObject.layer = (int)EnumLayers.Player;

//		// Enable camera following again
//		if( followingCameraComponent != null ) {
//			followingCameraComponent.enabled = true;
//		}

		// Get ready for another explosion!
		explosion.SetActive ( false );

		// Respawning has finished
		isRespawning = false;
	}

	public void UpgradeCannon ()
	{
		int indexOfCurrentCannon = fireSpots.IndexOf (currentFireSpot);
		if( indexOfCurrentCannon < (fireSpots.Count - 1) ) {
			indexOfCurrentCannon++;
			currentFireSpot = fireSpots[indexOfCurrentCannon];
		}
	}

	public List<GameObject> GetCurrentCannons() {
		List<GameObject> cannons = new List<GameObject>();
		foreach (Transform child in currentFireSpot.transform) {
			cannons.Add (child.gameObject);
		}
		return cannons;
	}

	private void Update () {

		// If player have just died
		if( isDead && !isRespawning && !GameManager.instance.gameOver ) {
			GameManager.instance.PlayerDied ();
			isRespawning = true;
			return;
		}

		// Controls the movement of the player
		transform.Translate(
			new Vector3 (
				Input.GetAxis ("Horizontal") * Time.deltaTime * playerSpeed,
				Input.GetAxis ("Vertical") * Time.deltaTime * playerSpeed,
				startPosition.z
			)
		);

		// Calculate boundaries, based on camera's orthographic limits!
		// Copy current position for further calculations
		Vector3 pos = transform.position;

		// Top boundary
		if( pos.y + playerRadius > mainCamera.orthographicSize ) {
			pos.y = mainCamera.orthographicSize - playerRadius;
		}
		// Bottom boundary
		if( pos.y - playerRadius < -mainCamera.orthographicSize ) {
			pos.y = -mainCamera.orthographicSize + playerRadius;
		}

		// Ratio is necessary to calculate horizontal boundaries
		float ratio = (float)Screen.width / (float)Screen.height;
		float orthoWidth = mainCamera.orthographicSize * ratio;

		// East boundary
		if( pos.x + playerRadius > orthoWidth ) {
			pos.x = orthoWidth - playerRadius;
		}
		// West boundary
		if( pos.x - playerRadius < -orthoWidth ) {
			pos.x = -orthoWidth + playerRadius;
		}

		// Ensure Z axis is not modified
		pos.z = startPosition.z;

		// Finally, set the new position
		transform.position = pos;
	}
}
