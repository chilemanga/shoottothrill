﻿using UnityEngine;
using System.Collections;

public class PowerUpController : MonoBehaviour {
	public int score = 1000;
	public float speedMultiplier = 0.0f;
	public bool cannonUpgrade = false;
	public int lifesToAdd = 0;
	public EnumPowerUpTypes powerUpType = EnumPowerUpTypes.PowerUp;
}
