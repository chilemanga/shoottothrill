﻿using UnityEngine;
using System.Collections;

public class OnTriggerEnter2DHandler : MonoBehaviour {

	/// <summary>
	/// This object health points. If you want to survive the impact, put a value greater than 1.
	/// </summary>
	public int hp = 1;

	#region Private fields and properties

	private bool imDead { get{ return hp <= 0; } }
	/// <summary>
	/// The stop checking flag. This field is necessary for objects that doesn't destroy right away after dying.
	/// </summary>
	private bool stopChecking = false;

	private PlayerController cachedPlayerController;
	private bool imPlayer
	{
		get
		{
			if( cachedPlayerController == null ) {
				cachedPlayerController = GetComponent<PlayerController>();
			}
			return cachedPlayerController != null;
		}
	}
	private EnemyController cachedEnemyController;
	private bool imEnemy
	{
		get
		{
			if( cachedEnemyController == null ) {
				cachedEnemyController = GetComponent<EnemyController>();
			}
			return cachedEnemyController != null;
		}
	}
	private PowerUpController cachedPowerUpController;
	private bool imPowerUp
	{
		get
		{
			if( cachedPowerUpController == null ) {
				cachedPowerUpController = GetComponent<PowerUpController>();
			}
			return cachedPowerUpController != null;
		}
	}

	#endregion

	void OnTriggerEnter2D (Collider2D other) {
		Debug.Log ("Trigger enter 2d!! hp="+hp.ToString());
		if( !imDead )
		{
			if( imPlayer && other.gameObject.GetComponent<PowerUpController>() ) {
				// avoid doing damage to player when taking a power up
				return;
			}
			hp--;
			// Checking death on Update to resolve all triggers before.
		}
	}
	
	void Update () {
		// Check if the object died every frame, but AFTER trigger collision to avoid one object dying before
		// the other, when all the objects involved in the collision should die if their hp runs out.
		if( imDead && !stopChecking )
		{
			if( imPowerUp ) {
				// Notify that a power up have been redeemed
				GameManager.instance.GrabbedAPowerUp ( cachedPowerUpController );
				Destroy (gameObject);
				return;
			}
			if( imEnemy ) {
				// Notify that an enemy has died
				GameManager.instance.EnemyDied ( cachedEnemyController );
				stopChecking = true;
				return;
			}
			if( !imPlayer && !imEnemy) {
				Destroy (gameObject);
			}
		}
	}
}
