﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnDirector : Singleton<SpawnDirector> {

	public List<GameObject> spawnPoints;
	public GameObject bossPrefab;
	public GameObject bossSpawnPoint;
	// I've decided to show up a boss every X amount of seconds.
	public float timerForBoss = 30f;

	// Boss info utilities
	public bool bossIsAlive = false;
	private OnTriggerEnter2DHandler bossHpHandler;
	public int bossHP
	{
		get
		{
			if( bossInstance != null ) {
				bossHpHandler = bossInstance.GetComponent<OnTriggerEnter2DHandler>();
			}
			if( bossHpHandler != null ) {
				return bossHpHandler.hp;
			}
			return 0;
		}
	}

	private float startingTimerForBoss;
	private GameObject bossInstance;
	private bool allSpawnPointsEnabled = true;

	private void Start () {
		startingTimerForBoss = timerForBoss;
	}

	private void EnableAllSpawnPoints ( bool shouldEnable ) {
		foreach (GameObject spawnPoint in spawnPoints) {
			if( spawnPoint != null ) {
				EntitySpawner ep = spawnPoint.GetComponent<EntitySpawner>();
				if( ep != null ) {
					ep.enabled = shouldEnable;
				}
			}
		}
		allSpawnPointsEnabled = shouldEnable;
	}

	private void SpawnBoss() {
		// Stop every spawn point
		EnableAllSpawnPoints (false);

		bossInstance = Instantiate(bossPrefab, bossSpawnPoint.transform.position, bossSpawnPoint.transform.rotation) as GameObject;
		timerForBoss = startingTimerForBoss;
		bossIsAlive = true;
	}
	
	void Update () {
		// Boss is dead (or never born)?
		if( bossInstance == null ) {
			bossIsAlive = false;
			timerForBoss -= Time.deltaTime;
			if( timerForBoss <= 0 && bossInstance == null ) {
				SpawnBoss();
				return;
			}

			// Start every spawn point again
			if( !allSpawnPointsEnabled ) {
				EnableAllSpawnPoints (true);
			}
		}
	}
}
