﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerUpSpawner : MonoBehaviour {

	public List<GameObject> powerUpPrefabs = new List<GameObject>();
	/// <summary>
	/// The chance to get power up. Set a value between 0 and 1.
	/// </summary>
	public float chanceToGetPowerUp = 0.5f;
	/// <summary>
	/// Set this to <c>false</c> if you want to trigger it manually.
	/// </summary>
	public bool fireOnEnable = true;
	
	private void OnEnable () {
		if( fireOnEnable ) {
			SpawnPowerUps();
		}
	}
	
	public void SpawnPowerUps ()
	{
		foreach (var powerUpPrefab in powerUpPrefabs) {
			// Per every powerup, pull the lever to see if we are lucky
			if( Random.Range(0f, 1f) <= chanceToGetPowerUp ) {
				Instantiate (powerUpPrefab, transform.position, transform.rotation);
			}
		}
	}
}
