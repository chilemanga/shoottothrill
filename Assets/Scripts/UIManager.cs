﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	#region Set in editor

	[Header ("Hud")]
	public GameObject topPanel;
	public Text livesText;
	public Text scoreText;
	public Text timeValueText;
	public Text bossHPText;
	[Header ("Pause and Options")]
	public GameObject pausePanel;
	[Header ("Game Over")]
	public GameObject gameOverPanel;
	public Text newHighScoreText;
	public Text currentHighScoreText;
	public AudioSource mainMusic;
	public AudioSource gameOverMusic;

	#endregion

	#region Util fields and properties

	private bool _isGamePaused = false;
	public bool isGamePaused
	{
		get
		{
			return _isGamePaused;
		}
		set
		{
			_isGamePaused = value;
			if( _isGamePaused ) {
				Time.timeScale = 0;
			} else {
				Time.timeScale = 1f;
			}
			pausePanel.SetActive( _isGamePaused );
		}
	}

	private bool gameOver = false;
	private string bossHPTextFormat;

	#endregion

	void Awake () {
		SetPlayerLivesText ();
		bossHPTextFormat = bossHPText.text;

		// Subscribe to game over and player died events
		GameManager.instance.OnGameOver += HandleOnGameOver;
		GameManager.instance.OnPlayerDied += HandleOnPlayerDied;
	}

	void SetPlayerLivesText ()
	{
		livesText.text = "Lives: " + GameManager.instance.playerLives;
	}

	void HandleOnPlayerDied ()
	{
		SetPlayerLivesText ();
	}

	void HandleOnGameOver ()
	{
		if( gameOverPanel != null ) {
			gameOverPanel.SetActive (true);
			gameOver = true;
			Time.timeScale = 0;

			// Set up all the score texts:
			if( GameManager.instance.newHighScoreAchieved ) {
				// Hide current highscore text...
				currentHighScoreText.text = string.Empty;

				// ...to emphasize new highscore achieved!
				newHighScoreText.text = string.Format( newHighScoreText.text, 
					GameManager.instance.currentHighScore,
				    GameManager.instance.currentHighScoreElapsedTime );
			} else {
				// Hide new highscore text...
				newHighScoreText.text = string.Empty;

				// ...to show how lame you are because you didn't beat it xD
				currentHighScoreText.text = string.Format( currentHighScoreText.text, 
					GameManager.instance.score, 
					((int)GameManager.instance.elapsedTime).ToString (), 
					GameManager.instance.currentHighScore,
					GameManager.instance.currentHighScoreElapsedTime);
			}

			// Switch music
			mainMusic.gameObject.SetActive(false);
			gameOverMusic.gameObject.SetActive(true);
		}
	}
	
	void Update () {
		// TODO: Update score subscribing to an event
		scoreText.text = "Score: " + GameManager.instance.score;
		// TODO: Update lifes subscribing to an event
		SetPlayerLivesText();

		// Show Boss HP when the boss is alive!
		if( SpawnDirector.instance.bossIsAlive ) {
			bossHPText.text = string.Format (bossHPTextFormat, SpawnDirector.instance.bossHP);
			bossHPText.gameObject.SetActive( true );
		} else if( !SpawnDirector.instance.bossIsAlive && bossHPText.gameObject.activeSelf ) {
			// if boss died, just hide boss hp text.
			bossHPText.gameObject.SetActive ( false );
		}

		timeValueText.text = ((int)GameManager.instance.elapsedTime).ToString ();

		# region Check pause/resume conditions

		if( !isGamePaused && Input.GetKeyDown (KeyCode.Escape) ) {
			// Enter pause
			isGamePaused = true;
		} else if( isGamePaused && Input.GetKeyDown (KeyCode.Escape) ) {
			// Resume game
			isGamePaused = false;
		} else if( isGamePaused && Input.GetKeyDown (KeyCode.Q) ) {
			// Go to main menu
			Time.timeScale = 1f;
			Application.LoadLevel ((int)EnumScenes.MainScene);
		}

		#endregion

		#region Check game over conditions

		if( gameOver && Input.GetKeyDown (KeyCode.Q) ) {
			Time.timeScale = 1;
			Application.LoadLevel ( (int)EnumScenes.MainScene );
		} else if ( gameOver && Input.GetKeyDown(KeyCode.Return) ) {
			// Reload game scene to start again
			Time.timeScale = 1;
			Application.LoadLevel( (int)EnumScenes.GameScene );
		}

		#endregion
	}
}
