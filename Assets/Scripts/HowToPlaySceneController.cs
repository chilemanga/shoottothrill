﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HowToPlaySceneController : MonoBehaviour {

	public Image upKeyImage;
	public Image downKeyImage;
	public Image leftKeyImage;
	public Image rightKeyImage;
	public Image spaceBarImage;
	public Color highLightColor = Color.yellow;

	void Update() {
		// Reset key colors for every key
		spaceBarImage.color = Color.white;
		upKeyImage.color = Color.white;
		downKeyImage.color = Color.white;
		leftKeyImage.color = Color.white;
		rightKeyImage.color = Color.white;

		if( Input.GetKey(KeyCode.Space) ) {
			spaceBarImage.color = highLightColor;
		}
		if( Input.GetKey(KeyCode.UpArrow) ) {
			upKeyImage.color = highLightColor;
		}
		if( Input.GetKey(KeyCode.DownArrow) ) {
			downKeyImage.color = highLightColor;
		}
		if( Input.GetKey(KeyCode.LeftArrow) ) {
			leftKeyImage.color = highLightColor;
		}
		if( Input.GetKey(KeyCode.RightArrow) ) {
			rightKeyImage.color = highLightColor;
		}

		if( Input.GetKeyDown(KeyCode.Return) ) {
			Application.LoadLevel( (int)EnumScenes.GameScene );
		}
		if( Input.GetKeyDown(KeyCode.Escape) ) {
			Application.LoadLevel( (int)EnumScenes.MainScene );
		}
	}
}
