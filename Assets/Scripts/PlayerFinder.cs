﻿using UnityEngine;
using System.Collections;

public class PlayerFinder : MonoBehaviour {
	private Transform _target;
	public Transform target
	{
		set { _target = value; }
		get
		{
			// Look directly into hierarchy
			if( _target == null ) {
				GameObject go = GameObject.FindWithTag ("Player");
				if( go != null ) { 
					_target = go.transform;
				} else {
					// try to find with name
					GameObject goName = GameObject.Find ("Player");
					if( goName != null ) {
						_target = goName.transform;
					}
				}
			}
			return _target;
		}
	}
}
