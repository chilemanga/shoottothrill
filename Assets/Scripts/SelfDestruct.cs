﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour {

	public float secondsAlive = 3f;

	void Update () {
		secondsAlive -= Time.deltaTime;
		if( secondsAlive <= 0 ) {
			Destroy( gameObject );
		}
	}
}
