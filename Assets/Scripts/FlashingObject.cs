﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlashingObject : MonoBehaviour {

	public float frequency = 1f;
	public GameObject targetToDeactivate;

	private float frequencyCounter;
	private bool isShowing;

	// This script tries to manipulate its "Image" alpha to make it flash.
	// If there's no Image on this gameObject, I try other approaches.
	private Image _cachedImageComponent;
	private bool _imageComponentFound = false;
	private Color currentImageColor;
	private Image currentImage
	{
		get
		{
			if( _cachedImageComponent == null && !_imageComponentFound ) {
				_cachedImageComponent = GetComponent<Image>();
				if( _cachedImageComponent != null ) {
					// This flag makes this script stop getting the component every time
					_imageComponentFound = true;
					currentImageColor = _cachedImageComponent.color;
				}
			}
			return _cachedImageComponent;
		}
	}
	// In case this gameObject doesn't have an Image to apply alpha, move it to the edge of the universe so it's not visible
	// I've used this approach because activating or deactivating it caused to stop calling Update method.
	// I also can't assume the object has a renderer, but the script will try to use this.
	private Vector3 currentNormalPosition;
	private Vector3 hidingPosition = new Vector3(-100000, -100000);
	private bool isUsingPositionApproach = false;

	void Start () {
		frequencyCounter = frequency;
		isShowing = targetToDeactivate != null ? targetToDeactivate.activeSelf : gameObject.activeSelf;
	}

	void OnEnable ()
	{
		// Make sure to move the object to its normal position when enabling it again
		// if just at the moment the object is enabled is not showing it, meaning
		// that is floating around in its "hiding" position.
		if( isUsingPositionApproach ) {
			currentNormalPosition = transform.position;
		}
	}

	void Update () {
		frequencyCounter -= Time.deltaTime;

		if( frequencyCounter <= 0 ) {
			if( targetToDeactivate != null ) {
				// Try to hide the explicit target
				targetToDeactivate.SetActive(!isShowing);
			} else if( renderer != null ) {
				// No explicit target? try to disable its renderer
				renderer.enabled = !renderer.enabled;
			} else if (currentImage) {
				// No renderer? try to hide its image
				currentImage.color = isShowing ? Color.clear : currentImageColor;
			} else {
				// No nothing!? Try to move to a hide position the object so it doesn't show in the screen
				if( isShowing ) {
					currentNormalPosition = transform.position;
				}
				transform.position = isShowing ? hidingPosition : currentNormalPosition;
				isUsingPositionApproach = true;
			}
			isShowing = !isShowing;
			frequencyCounter = frequency;
		}
	}
}
