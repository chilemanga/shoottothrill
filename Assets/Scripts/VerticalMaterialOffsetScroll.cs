﻿using UnityEngine;
using System.Collections;

public class VerticalMaterialOffsetScroll : MonoBehaviour {

	/// <summary>
	/// The scroll sluginess. The higher the value, slower the scroll will move.
	/// </summary>
	public float scrollSluginess;
	
	void Update () {
		Vector2 newOffset = renderer.material.mainTextureOffset;
		newOffset.y += Time.deltaTime / scrollSluginess;
		renderer.material.mainTextureOffset = newOffset;
	}
}
