﻿using UnityEngine;
using System.Collections;

public class FollowHorizontalSmoothly : MonoBehaviour {

	public GameObject target;
	private PlayerFinder playerFinderComponent;

	void Start () {
		// No target set? No problem! Let's find our target with playerFinder
		if( target == null ) {
			playerFinderComponent = GetComponent<PlayerFinder>();
			if( playerFinderComponent != null && playerFinderComponent.target != null ) {
				target = playerFinderComponent.target.gameObject;
			}
		}
	}

	void Update () {
		if( target != null )
		{
			Vector3 targetPosition = target.transform.position;
			// We just want to follow horizontal position, so save Z and Y axis as it is.
			targetPosition.z = transform.position.z;
			targetPosition.y = transform.position.y;

			// Do the smooth follow up
			transform.position = Vector3.Lerp (transform.position, targetPosition, Time.deltaTime);
		}
	}
}
