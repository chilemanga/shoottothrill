﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManager {

	private const string fileName = "/shootToThrillData.dat";

	public static void Save (DataContainer data) {
		string filePath = Application.persistentDataPath + fileName;
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open (filePath, FileMode.Create);

		bf.Serialize (file, data);
		file.Close ();
	}

	public static DataContainer Load () {
		string filePath = Application.persistentDataPath + fileName;
		if( File.Exists (filePath) ) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open (filePath, FileMode.Open);
			DataContainer data = bf.Deserialize (file) as DataContainer;
			file.Close ();

			return data;
		}
		return null;
	}
}

[Serializable]
public class DataContainer {
	public int highscore;
	public int elapsedTime;
}
