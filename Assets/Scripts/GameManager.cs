﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager> {

	#region Fields set in editor

	public int playerLives = 3;
	public PlayerController playerController;

	#endregion

	#region Util Properties

	[HideInInspector]
	public int score;

	private DataContainer gameData;
	public int currentHighScore
	{
		get
		{
			if( gameData != null ) {
				return gameData.highscore;
			}
			return 0;
		}
		set
		{
			if( gameData == null ) {
				gameData = new DataContainer();
			}
			gameData.highscore = value;
		}
	}
	public int currentHighScoreElapsedTime
	{
		get
		{
			if( gameData != null ) {
				return gameData.elapsedTime;
			}
			return 0;
		}
		set
		{
			if( gameData == null ) {
				gameData = new DataContainer();
			}
			gameData.elapsedTime = value;
		}
	}
	[HideInInspector]
	public bool newHighScoreAchieved = false;

	[HideInInspector]
	public float elapsedTime = 0;
	public bool gameOver {
		get {
			return playerLives <= 0;
		}
	}

	#endregion

	#region Events and delegates

	public delegate void OnGameOverHandler();
	public event OnGameOverHandler OnGameOver;

	public delegate void OnPlayerDiedHandler();
	public event OnPlayerDiedHandler OnPlayerDied;

	public delegate void OnEnemyDiedHandler(EnemyController enemyController);
	public event OnEnemyDiedHandler OnEnemyDied;

	#endregion

	#region Methods

	private void Awake () {
		// Load game saved data, if available
		gameData = DataManager.Load ();
	}

	public void PlayerDied() {
		--playerLives;

		if( OnPlayerDied != null ) {
			OnPlayerDied();
		}
		// No more lives? Fire game over event
		if( gameOver ) {

			// A new highscore have been made! Woohoo!
			if( score > currentHighScore ) {
				currentHighScore = score;
				currentHighScoreElapsedTime = (int)elapsedTime;
				newHighScoreAchieved = true;

				// Save this epic moment in a file so we can brag about it later!
				DataManager.Save (gameData);
			}

			if( OnGameOver != null ) {
				OnGameOver();
			}
		}
		//elapsedTime = 0;
	}

	public void EnemyDied(EnemyController enemyController) {
		score += enemyController.score;

		if( OnEnemyDied != null ) {
			OnEnemyDied( enemyController );
		}
	}

	/// <summary>
	/// Applies all the goodies that a powerup can have.
	/// </summary>
	/// <param name="cachedPowerUpController">The power up obtained.</param>
	public void GrabbedAPowerUp (PowerUpController cachedPowerUpController)
	{
		playerLives += cachedPowerUpController.lifesToAdd;
		score += cachedPowerUpController.score;
		if( cachedPowerUpController.speedMultiplier > 0 ) {
			playerController.playerSpeed *= cachedPowerUpController.speedMultiplier;
		}
		if( cachedPowerUpController.cannonUpgrade ) {
			playerController.UpgradeCannon();
		}
	}

	void Update()
	{
		if( !gameOver ) {
			elapsedTime += Time.deltaTime;
		}
	}

	#endregion
}
