Shoot To Thrill
===============

A vertical scrolling shoot'em up game made in Unity.

Requirements
============

- Unity 4.6.6


Getting started
===============

1. Open the project.
2. Go to _MainScene (at the root of "Assets" folder).
3. Hit play button in Unity.
4. Follow on screen instructions.
5. Have fun!

*Note:* - Game have been made for learning / sharing purposes.

Techniques used
===============

- Material offset for infinite vertical scroll.
- Single component parallax for any number of parallax layers (set in editor).
- Reusable behaviors for moving, rotating, shooting, colliding.
- Resolution should be fixed, at 600x900 (upright).

I hope you have fun playing!
